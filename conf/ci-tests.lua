tisrCiTest=TimedIPSetRule()
tisrRest=TimedIPSetRule()
addAction(tisrCiTest:slice(), PoolAction("ci-test"))
addAction(tisrRest:slice(), PoolAction("Me-Me-MEEE"))

dofile('/etc/dnsdist/conf/ciBots.lua')

function pickPool(dq)
        if(citestPeople:match(dq.remoteaddr))
        then
                print("Lua caught query for a ciTestBot")
                tisrCiTest:add(dq.remoteaddr, 1)
                return DNSAction.Pool, "ci-test"
        else
                print("Lua caught query for restPerson")
                tisrRest:add(dq.remoteaddr, 1)
                return DNSAction.Pool, "Me-Me-MEEE"
        end
end


--[[
Based on https://dnsdist.org/advanced/timedipsetrule.html
--]]

--[[
This program is free software: you can redistribute it and/or modify
it under the terms of the modified GNU Affero General Public License as
published by the My Privacy DNS, either version 3 of the
License, or any later version released at 
https://www.mypdns.org/w/License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTYwithout even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
modified GNU Affero General Public License for more details.

You should have received a copy of the modified GNU Affero General Public License
along with this program. If not, see https://www.mypdns.org/w/License.

The modification: The standard AGPLv3 have been altered to NOT allow
any to generate profit from our work. You are however free to use it to any
NON PROFIT purpose. If you would like to use any of our code for profiteering
YOU are obliged to contact https://www.mypdns.org/ for profit sharing.
--]]
